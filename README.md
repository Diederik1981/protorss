# README #

This readme assumes you use a mac or linux, windows commands should be similar
### What is this repository for? ###

* ProtoRSS is a very lean RSS client intended to serve your metadata needs.

### Run Tests ###
* From root: ./gradlew test 

### Run application from command line ###
* From root: ./gradlew bootRun

### Run application from eclipse ( minimum 2018-12 i guess ) ###
* install lombok from: https://projectlombok.org/download
* import project and run

### Ways to use application after running it ###
* Open localhost:8080 for the welcome page with statisticsc 
* add channel: curl -X POST -H "Content-Type: application/json" -d '<rss_feed_url>' localhost:8080/v1/rss/channel

### Access H2-Database while running ###
* open localhost:8080/h2-console
* username: sa
* password: sa
* JDBC url: jdbc:h2:./h2database

### Sites with channels that are 'protoRSS' compatible ###
* http://feeds.skynews.com/feeds/rss/uk.xml
* https://www.channelnewsasia.com/rssfeeds/8395986 - has categories
* http://rss.cnn.com/rss/cnn_topstories.rss 
* http://feeds.bbci.co.uk/news/world/rss.xml
* http://feeds.bbci.co.uk/news/rss.xml

### Api endpoints ###  
* POST http://localhost:8080/v1/rss/channel - with valid (maybe one of the above) endpoints
* GET http://localhost:8080/v1/rss/channel/?pageNo=1&pageSize=10&descriptionStartsWith=start&descriptionContains=xcontians&titleStartsWith=startwi
* GET http://localhost:8080/v1/rss/channel/item?pageNo=1&pageSize=10&descriptionStartsWith=M&descriptionContains=or&titleStartsWith=C
* GET http://localhost:8080/v1/rss/channel/item/stats?descriptionStartsWith=A&descriptionContains=or
