package protorss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProtoRssApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProtoRssApplication.class, args);
	}

}
