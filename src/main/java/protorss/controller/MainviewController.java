package protorss.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import protorss.dto.RssFeedDto;
import protorss.dto.RssItemDto;
import protorss.service.RssFeedService;
import protorss.service.RssStatisticsService;

@Controller
public class MainviewController {
	
	@Autowired
	private RssFeedService rssFeedService;

    @GetMapping("/")
    public String main(Model model) {
    	
    	List<RssFeedDto> allFeeds = rssFeedService.getAllFeeds();
    	List<RssItemDto> allItems = rssFeedService.getAllItems(null, null);
    	allItems.stream().flatMap(item -> item.getCategories().stream()).distinct().count();
    	allItems.stream().flatMap(item -> item.getCategories().stream()).distinct().count();

    	model.addAttribute("nrOfChannels", allFeeds.size());
        model.addAttribute("nrOfItems", allItems.size());
        model.addAttribute("nrOfCategories", allItems.stream().flatMap(item -> item.getCategories().stream()).count());
        model.addAttribute("nrOfDistinctCategories", allItems.stream().flatMap(item -> item.getCategories().stream()).distinct().count());

        return "welcome"; //view
    }
}
