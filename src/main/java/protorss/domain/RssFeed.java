package protorss.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.rometools.rome.feed.synd.SyndFeed;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Main RSS entity, it stores the global information for an rss feed.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="RSSFEED")
public class RssFeed {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * This is used to identify a channel for all RSS versions including 2.0.
	 */
	@Column(name = "LINK", nullable = false, unique=true)
	private String link;
	
	@Column(name = "TITLE", nullable = false)
	private String title;
	
	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	@Builder.Default
	@OneToMany(
			targetEntity = RssItem.class, 
			//mappedBy = "rssFeed", 
			fetch = FetchType.LAZY,
			cascade = CascadeType.ALL
	)
	private List<RssItem> items = new ArrayList<>();

	public static RssFeed from(SyndFeed feed) {
		RssFeed rssFeed = RssFeed.builder()
				.link(feed.getLink())
				.title(feed.getTitle())
				.description(feed.getDescription())
				.build();

		rssFeed.setItems(feed.getEntries()
				.stream()
				.map(e -> RssItem.from(e, rssFeed))
				.collect(Collectors.toList()));
		return rssFeed;
	}

}
