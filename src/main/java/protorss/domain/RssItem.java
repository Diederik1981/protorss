package protorss.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.rometools.rome.feed.synd.SyndEntry;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * One RSS item from an RSS channel.
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="RSSITEM")
public class RssItem {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "LINK", nullable = false)
	private String link;
	
	@Column(name = "TITLE", nullable = false)
	private String title;
	
	@Column(name = "DESCRIPTION", nullable = false)
	private String description;
	
	@ManyToOne	
	private RssFeed rssFeed;
	
	@Builder.Default
	@OneToMany(targetEntity = RssItemCategory.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<RssItemCategory> categories = new ArrayList<>();


	/**
	 * Creates an item from an entry in a feed. 
	 */
	static RssItem from(SyndEntry entry, RssFeed feed) {
		RssItem item = RssItem.builder()
				.link(entry.getLink())
				.title(entry.getTitle())
				.description(entry.getDescription() == null ? null : entry.getDescription().getValue())
				.rssFeed(feed)
				.build();
		item.setCategories(entry.getCategories()
				.stream().map(cat -> RssItemCategory.from(cat.getName(), item))
				.collect(Collectors.toList()));
		return item;
	}

}