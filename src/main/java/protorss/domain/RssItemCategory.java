package protorss.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Stores a category name for an RSS item.
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="RSSITEMCATEGORY")
public class RssItemCategory {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * The name of the category.
	 */
	@Column(name = "NAME", nullable = false)
	private String name; 
	
	@ManyToOne	
	private RssItem rssItem;
	
	
	static RssItemCategory from(String name, RssItem item) {
		return RssItemCategory.builder()
				.name(name)
				.rssItem(item)
				.build();
	}

}
