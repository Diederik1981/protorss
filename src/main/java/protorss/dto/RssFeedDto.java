package protorss.dto;

import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import protorss.domain.RssFeed;

@Data
@Builder
@AllArgsConstructor
public class RssFeedDto {
	private final Long id;
	private final String link;
	private final String title;
	private final String description;
	private final List<RssItemDto> items;
	
	public static RssFeedDto from(RssFeed feed) {
		return RssFeedDto.builder()
			.id(feed.getId())
			.link(feed.getLink())
			.title(feed.getTitle())
			.description(feed.getDescription())
			.items(feed.getItems()
					.stream()
					.map(RssItemDto::from)
					.collect(Collectors.toUnmodifiableList()))
			.build();
	}
}
