package protorss.dto;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Builder;
import lombok.Data;
import protorss.domain.RssItem;
import protorss.domain.RssItemCategory;

@Data
@Builder
public class RssItemDto {
	private final Long id;
	private final String link;
	private final String title;
	private final String description;
	private final List<String> categories;
	
	public static RssItemDto from(RssItem item) {
		return RssItemDto.builder()
			.id(item.getId())
			.link(item.getLink())
			.title(item.getTitle())
			.description(item.getDescription())
			.categories(item.getCategories()
					.stream()
					.map(RssItemCategory::getName)
					.collect(Collectors.toUnmodifiableList()))
			.build();
	}
}
