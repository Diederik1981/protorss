package protorss.dto.predicates;

import java.util.function.BiPredicate;

import protorss.dto.RssFeedDto;

/**
 * A class with predicates for 'RssFeedDto'.
 */
public class RssFeedDtoPredicates {
	
	private RssFeedDtoPredicates() {
	}
	
	public static final BiPredicate<RssFeedDto, String> DESCRIPTION_STARTS_WITH = (rssFeed, prefix) -> rssFeed.getDescription().toLowerCase().startsWith(prefix.toLowerCase());
	public static final BiPredicate<RssFeedDto, String> DESCRIPTION_CONTAINS = (rssFeed, prefix) -> rssFeed.getDescription().toLowerCase().indexOf(prefix.toLowerCase()) != -1;
	
	public static final BiPredicate<RssFeedDto, String> TITLE_STARTS_WITH = (rssFeed, prefix) -> rssFeed.getTitle().toLowerCase().startsWith(prefix.toLowerCase());
	public static final BiPredicate<RssFeedDto, String> TITLE_CONTAINS = (rssFeed, prefix) -> rssFeed.getTitle().toLowerCase().indexOf(prefix.toLowerCase()) != -1;
	
}
