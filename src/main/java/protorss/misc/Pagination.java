package protorss.misc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Class to store pagination. Page numbering starts with 1.
 */
@Data
@Builder
@AllArgsConstructor
public class Pagination {
	
	@Builder.Default
	public static final int MAX_PAGE_SIZE = 4000;
	public static final Pagination DEFAULT = Pagination.of(1, MAX_PAGE_SIZE);

	private final int pageNo;
	private final int pageSize;

	/**
	 * Store pagination. 
	 * @param pageNo Assumes 1 of higher and subtracts 1.
	 * @param pageSize Assumes 0 < pageSize < 4000 corrects otherwise to nearest valid value.
	 * @return pagination
	 */
	public static Pagination of(int pageNo, int pageSize) {
		return of(pageNo, pageSize, null);
	}

	/**
	 * Store pagination. 
	 * @param pageNo Assumes 1 of higher and subtracts 1.
	 * @param pageSize Assumes 0 < pageSize < 4000 corrects otherwise to nearest valid value.
	 * @param maxPageSize pageSize must be smaller than this size.
	 * @return pagination
	 */
	public static Pagination of(int pageNo, int pageSize, Integer maxPageSize) {
		pageNo--;
		if (maxPageSize == null) {
			maxPageSize = MAX_PAGE_SIZE;
		} else if (maxPageSize < 1) {
			maxPageSize = 1;
		}
		return Pagination.builder()
				.pageNo(clampPageNo(pageNo))
				.pageSize(clampPageSize(pageSize, maxPageSize))
				.build();
	}
	
	public int getSkip() {
		return pageNo * pageSize; 
	}
	
	private static int clampPageNo(int pageNo) {
		if (pageNo < 0) {
			pageNo = 0;
		}
		return pageNo;
	}

	private static int clampPageSize(int pageSize, int maxPageSize) {
		if (pageSize < 1) {
			pageSize = 1;
		}
		if (pageSize > maxPageSize) {
			pageSize = maxPageSize;
		}
		return pageSize;
	}
}
