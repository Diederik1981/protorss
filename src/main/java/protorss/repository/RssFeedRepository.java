package protorss.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import protorss.domain.RssFeed;

@Repository
public interface RssFeedRepository extends JpaRepository<RssFeed, Long> {
	Optional<RssFeed> findByLink(String link);
	
	@Query(value = "SELECT  COUNT(DISTINCT NAME) FROM RssItemCategory ")
	long countDistinctCategories();
	
	@Query(value = "SELECT  COUNT(*) FROM RssItemCategory ")
	long countCategories();
	
//	@Query(value = "SELECT AVG(CHAR_LENGTH(Title)) AS avgLength FROM RssFeed")
//	long avgTitleLenght();
//	
//	@Query(value = "SELECT AVG(CHAR_LENGTH(Description)) AS avgLength FROM RssFeed")
//	long avgDescriptionLenght();
	
}