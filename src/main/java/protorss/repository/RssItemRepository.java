package protorss.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import protorss.domain.RssItem;

public interface RssItemRepository extends JpaRepository<RssItem, Long> {

}
