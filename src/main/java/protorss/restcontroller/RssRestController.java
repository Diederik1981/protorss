package protorss.restcontroller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import protorss.dto.RssFeedDto;
import protorss.dto.RssItemDto;
import protorss.dto.predicates.RssFeedDtoPredicates;
import protorss.dto.predicates.RssItemDtoPredicates;
import protorss.misc.Pagination;
import protorss.service.RssFeedService;
import protorss.service.exception.ImportRssFeedHeaderRequiredFieldMissingException;
import protorss.service.exception.ReadRssFeedMalformedURLException;
import protorss.service.exception.RssChannelAlreadyAddedException;

@RestController
@RequestMapping("v1/rss")
public class RssRestController {

	@Autowired
	private RssFeedService rssFeedService;

	//TODO: write test
	@PostMapping("/channel")
	public ResponseEntity<Object> postChannel(@NotNull @RequestBody String channel) {
		try {
			rssFeedService.addChannel(channel);
			return ResponseEntity.ok(channel);
		} catch (RssChannelAlreadyAddedException | 
				ReadRssFeedMalformedURLException | 
				ImportRssFeedHeaderRequiredFieldMissingException exc) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exc.getMessage(), null);
		}
	}

	//TODO: write test	
	@GetMapping("/channel")
	public ResponseEntity<Object> getFeeds(
			@RequestParam Optional<String> descriptionStartsWith,
			@RequestParam Optional<String> descriptionContains,
			@RequestParam Optional<String> titleStartsWith,
			@RequestParam Optional<String> titleContains,
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) {
		
		Map<BiPredicate<RssFeedDto, String>, String> filter = new HashMap<>();
		descriptionStartsWith.ifPresent(str -> filter.put(RssFeedDtoPredicates.DESCRIPTION_STARTS_WITH, str));
		descriptionContains.ifPresent(str -> filter.put(RssFeedDtoPredicates.DESCRIPTION_CONTAINS, str));
		titleStartsWith.ifPresent(str -> filter.put(RssFeedDtoPredicates.TITLE_STARTS_WITH, str));
		titleContains.ifPresent(str -> filter.put(RssFeedDtoPredicates.TITLE_CONTAINS, str));
		
		return ResponseEntity.ok(rssFeedService.getAllFeeds(Pagination.of(pageNo, pageSize), filter));
	}
	
	
	// TODO: write test
	@GetMapping("/channel/item")
	public ResponseEntity<Object> getItems(
			@RequestParam Optional<String> descriptionStartsWith,
			@RequestParam Optional<String> descriptionContains,
			@RequestParam Optional<String> titleStartsWith,
			@RequestParam Optional<String> titleContains,
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) {
		
		Map<BiPredicate<RssItemDto, String>, String> filter = new HashMap<>();
		descriptionStartsWith.ifPresent(str -> filter.put(RssItemDtoPredicates.DESCRIPTION_STARTS_WITH, str));
		descriptionContains.ifPresent(str -> filter.put(RssItemDtoPredicates.DESCRIPTION_CONTAINS, str));
		titleStartsWith.ifPresent(str -> filter.put(RssItemDtoPredicates.TITLE_STARTS_WITH, str));
		titleContains.ifPresent(str -> filter.put(RssItemDtoPredicates.TITLE_CONTAINS, str));
		
		return ResponseEntity.ok(rssFeedService.getAllItems(Pagination.of(pageNo, pageSize), filter));
	}
	
	// TODO: write test
	@GetMapping("/channel/item/stats")
	public ResponseEntity<Object> getItemsStats(
			@RequestParam Optional<String> descriptionStartsWith,
			@RequestParam Optional<String> descriptionContains,
			@RequestParam Optional<String> titleStartsWith,
			@RequestParam Optional<String> titleContains,
			@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize) {
		
		Map<BiPredicate<RssItemDto, String>, String> filter = new HashMap<>();
		descriptionStartsWith.ifPresent(str -> filter.put(RssItemDtoPredicates.DESCRIPTION_STARTS_WITH, str));
		descriptionContains.ifPresent(str -> filter.put(RssItemDtoPredicates.DESCRIPTION_CONTAINS, str));
		titleStartsWith.ifPresent(str -> filter.put(RssItemDtoPredicates.TITLE_STARTS_WITH, str));
		titleContains.ifPresent(str -> filter.put(RssItemDtoPredicates.TITLE_CONTAINS, str));
		List<RssItemDto> items = rssFeedService.getAllItems(Pagination.of(pageNo, pageSize), filter);
		Map<String,Object> stats = new HashMap<>(); 
		stats.put("titleLengthStats", items.stream().mapToInt(i -> i.getTitle().length()).summaryStatistics());
		stats.put("descriptionLengthStats", items.stream().mapToInt(i -> i.getDescription().length()).summaryStatistics());
		stats.put("linkLengthStats", items.stream().mapToInt(i -> i.getLink().length()).summaryStatistics());
		stats.put("averageDescriptionWordCount", items.stream().mapToInt(i -> i.getDescription().split(" ").length).average());
		stats.put("usedCategoryCount", items.stream().flatMap(item -> item.getCategories().stream()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));
		stats.put("usedDescriptionWordCount", items.stream().flatMap(item -> Arrays.asList(item.getDescription().split(" ")).stream()).map(str -> str.toLowerCase()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));
		
		
		return ResponseEntity.ok(stats);
	}
}
