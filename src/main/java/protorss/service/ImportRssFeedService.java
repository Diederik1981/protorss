package protorss.service;

import protorss.domain.RssFeed;

public interface ImportRssFeedService {

	RssFeed readUrl(String url);

}
