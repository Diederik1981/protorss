package protorss.service;

import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;

import protorss.domain.RssFeed;
import protorss.dto.RssFeedDto;
import protorss.dto.RssItemDto;
import protorss.misc.Pagination;

public interface RssFeedService {
	
	/**
	 * Add a RSS channel by URL.
	 * @param channel URL for the channel you want to add.
	 * @return RssFeed for given channel
	 */
	public RssFeed addChannel(String channel);
	public List<RssFeedDto> getAllFeeds();
	public List<RssFeedDto> getAllFeeds(Pagination p, Map<BiPredicate<RssFeedDto, String>, String> filter);
	public List<RssItemDto> getAllItems(Pagination p, Map<BiPredicate<RssItemDto, String>, String> filter);
	
}
