package protorss.service;

public interface RssStatisticsService {
	
	long getNumberOfChannels();
	double getAvgChannelTitleLenght();
	double getAvgChannelDescriptionLenght();
	
	long getNumberOfItems();
	long getDistinctNumberOfCategories();
	long getNumberOfCategories();
}
