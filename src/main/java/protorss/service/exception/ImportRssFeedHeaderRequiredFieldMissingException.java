package protorss.service.exception;

public class ImportRssFeedHeaderRequiredFieldMissingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ImportRssFeedHeaderRequiredFieldMissingException(String fieldName) {
		super(String.format("Required RSS-Channel field: '%s' is missing", fieldName));
	}
}
