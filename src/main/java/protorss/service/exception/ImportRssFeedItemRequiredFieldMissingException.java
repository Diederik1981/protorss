package protorss.service.exception;

public class ImportRssFeedItemRequiredFieldMissingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ImportRssFeedItemRequiredFieldMissingException(String fieldName) {
		super(String.format("Required RSS-Item field: '%s' is missing", fieldName));
	}
	
	
}
