package protorss.service.exception;

public class ReadRssFeedMalformedURLException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public ReadRssFeedMalformedURLException(String url) {
		super(String.format("The RSS URL :'%s' is malformed", url));
	}
}
