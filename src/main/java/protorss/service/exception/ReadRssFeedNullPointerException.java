package protorss.service.exception;

public class ReadRssFeedNullPointerException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ReadRssFeedNullPointerException() {
		super("RSS URL may not be 'null'.");
	}

}
