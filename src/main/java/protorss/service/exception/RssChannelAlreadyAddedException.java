package protorss.service.exception;

public class RssChannelAlreadyAddedException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public RssChannelAlreadyAddedException(String url) {
		super(String.format("Cannot add because RSS channel with URL: '%s' already exists.", url));
	}
}
