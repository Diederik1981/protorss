package protorss.service.helper;

import java.net.URL;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import protorss.domain.RssFeed;
import protorss.service.exception.ReadRssFeedMalformedURLException;
import protorss.service.exception.ReadRssFeedNullPointerException;

/**
 * A helper for retrieving an 'RSS feed' for a given 'URL'.
 */
public class ReadRssFeedHelper {
	
	private ReadRssFeedHelper () {} // Non instanciable helper constructor.

	/**
	 * Retrieves an 'RSS feed' for the given 'url'.
	 */
	public static RssFeed readFeed(String url) {
		if (url == null) {
			throw new ReadRssFeedNullPointerException();
		}
		SyndFeed feed = readXMLfromURL(url);		
		feed.setLink(url);
		return toDomain(feed);
	}
	
	/**
	 * Retrieving the 'Rss xml' from the given 'URL' endpoint. 
	 */
	private static SyndFeed readXMLfromURL(String url) {
		SyndFeed feed;
		try {
			try (XmlReader reader = new XmlReader(new URL(url))) {
				feed = new SyndFeedInput().build(reader);
			}
		} catch (Exception e) {
			throw new ReadRssFeedMalformedURLException(url);
		}
		return feed;
	}

	/**
	 * Converts an incoming 'RSS feed' to our 'domain'.
	 */
	public static RssFeed toDomain(SyndFeed syndFeed) {
		return RssFeed.from(syndFeed);
	}

	/**
	 * This is here for debugging. 
	 */
	public static void main(String[] args) {
		//RssFeed readFeed = ReadRssFeedHelper.readFeed("https://www.channelnewsasia.com/rssfeeds/8395986");
		
		SyndFeed feed = readXMLfromURL("https://www.channelnewsasia.com/rssfeeds/8395986");
		feed.getEntries().stream().forEach(e -> System.out.println(e.getPublishedDate()));
		
		//System.out.println(readFeed);
	}
}
