package protorss.service.impl;

import org.springframework.stereotype.Service;

import protorss.domain.RssFeed;
import protorss.service.ImportRssFeedService;
import protorss.service.helper.ReadRssFeedHelper;
import protorss.service.validator.RssFeedValidator;

@Service
public class ReadRssFeedServiceImpl implements ImportRssFeedService {
	
	@Override
	public RssFeed readUrl(String url) {
		 RssFeed readFeed = ReadRssFeedHelper.readFeed(url);
		 RssFeedValidator.validate(readFeed);
		 return readFeed;
	}
	
}
