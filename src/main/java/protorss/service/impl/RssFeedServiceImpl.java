package protorss.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import protorss.domain.RssFeed;
import protorss.dto.RssFeedDto;
import protorss.dto.RssItemDto;
import protorss.misc.Pagination;
import protorss.repository.RssFeedRepository;
import protorss.service.RssFeedService;
import protorss.service.exception.RssChannelAlreadyAddedException;
import protorss.service.helper.ReadRssFeedHelper;

@Service
public class RssFeedServiceImpl implements RssFeedService {
	private FeedCache feedCache = new FeedCache();

	@Autowired
	RssFeedRepository rssRepository;

	RssFeed save(RssFeed entity) {
		 rssRepository.saveAndFlush(entity);
		 feedCache.putFeed(RssFeedDto.from(entity));
		 return entity;
	}

	/**
	 * Adds an 'RSS' channel by 'url'.
	 */
	@Override
	public RssFeed addChannel(String channel) {
		rssRepository.findByLink(channel).ifPresent(rss -> {
			throw new RssChannelAlreadyAddedException(channel);
		});

		RssFeed feed = ReadRssFeedHelper.readFeed(channel);
		save(feed);
		return feed;
	}

	@Override
	public List<RssFeedDto> getAllFeeds() {
		return getAllFeeds(null, null);
	}

	@Override
	public List<RssFeedDto> getAllFeeds(Pagination pagination, Map<BiPredicate<RssFeedDto, String>, String> filter) {
		if (pagination == null) {
			pagination = Pagination.DEFAULT;
		}

		return feedCache.get()
				.filter(feed -> filter == null ? true : filter.entrySet().stream().map(entry -> entry.getKey().test(feed, entry.getValue()))
								.noneMatch(pre -> pre.equals(Boolean.FALSE)))
				.skip(pagination.getSkip())
				.limit(pagination.getPageSize())
				.collect(Collectors.toList());
	}
	
	@Override
	public List<RssItemDto> getAllItems(Pagination pagination, Map<BiPredicate<RssItemDto, String>, String> filter) {
		if (pagination == null) {
			pagination = Pagination.DEFAULT;
		}
		return feedCache.get().flatMap(feed -> feed.getItems().stream())
				.filter(item -> filter == null ? true : filter.entrySet()
						.stream()
						.map(entry -> entry.getKey().test(item, entry.getValue()))
						.noneMatch(pre -> pre.equals(Boolean.FALSE)))
				.skip(pagination.getSkip())
				.limit(pagination.getPageSize())
				.collect(Collectors.toList());
	}

	final class FeedCache {
		private final Map<String, RssFeedDto> cache = new ConcurrentHashMap<>();
		private volatile boolean isInvalidated = true;
		private FeedCache() {
		}

		public synchronized Stream<RssFeedDto> get() {
			if (isInvalidated) {
				cache.clear();
				rssRepository.findAll()
					.stream()
					.map(RssFeedDto::from)
					.forEach(feedDto -> cache.put(feedDto.getTitle(), feedDto));
				isInvalidated = false;
			}
			return cache.values().stream();
		}
		
		public synchronized void putFeed(RssFeedDto rssFeedDto) {
			if (isInvalidated) {
				get();
			}
			cache.put(rssFeedDto.getTitle(), rssFeedDto);
		}
	}



}
