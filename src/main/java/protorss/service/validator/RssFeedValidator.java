package protorss.service.validator;

import protorss.domain.RssFeed;
import protorss.domain.RssItem;
import protorss.service.exception.ImportRssFeedHeaderRequiredFieldMissingException;
import protorss.service.exception.ImportRssFeedItemRequiredFieldMissingException;

public class RssFeedValidator {

	private RssFeedValidator() {} // Non instanciable constructor.

	/**
	 * Basic validation for channel.
	 */
	public static void validate(RssFeed feed) {
		if (feed.getLink() == null) {
			throw new ImportRssFeedHeaderRequiredFieldMissingException("link");
		}
		if (feed.getTitle() == null) {
			throw new ImportRssFeedHeaderRequiredFieldMissingException("title");
		}
		if (feed.getDescription() == null) {
			throw new ImportRssFeedHeaderRequiredFieldMissingException("description");
		}
		feed.getItems().stream().forEach(RssFeedValidator::validate);
	}

	/**
	 * Basic validation for item.
	 */
	public static void validate(RssItem item) {
		if (item.getLink() == null) {
			throw new ImportRssFeedItemRequiredFieldMissingException("link");
		}
		if (item.getTitle() == null) {
			throw new ImportRssFeedItemRequiredFieldMissingException("title");
		}
		if (item.getDescription() == null) {
			throw new ImportRssFeedItemRequiredFieldMissingException("description");
		}
	}
}
