package protorss.dto.predicates;

import static org.assertj.core.api.Assertions.assertThat;
import static protorss.dto.predicates.RssItemDtoPredicates.DESCRIPTION_CONTAINS;
import static protorss.dto.predicates.RssItemDtoPredicates.DESCRIPTION_STARTS_WITH;
import static protorss.dto.predicates.RssItemDtoPredicates.TITLE_CONTAINS;
import static protorss.dto.predicates.RssItemDtoPredicates.TITLE_STARTS_WITH;

import org.junit.Test;

import protorss.dto.RssItemDto;

/**
 * Test for 'RssItemDto' predicates
 */
public class RssFeedDtoPredicatesTest {
	
	/**
	 * The Dto that will be used for all predicates.
	 */
	public static final RssItemDto rssItemDto = RssItemDto.builder()
			.description("Description")
			.link("Link")
			.title("Title")
			.build();

	@Test
	public void testDescriptionStartsWith() {
		assertThat(DESCRIPTION_STARTS_WITH.test(rssItemDto, "Desc")).isTrue();
		assertThat(DESCRIPTION_STARTS_WITH.test(rssItemDto, "esc")).isFalse();
		assertThat(DESCRIPTION_STARTS_WITH.test(rssItemDto, "desc")).isTrue();
	}

	@Test
	public void testDescriptionContains() {
		assertThat(DESCRIPTION_CONTAINS.test(rssItemDto, "Desc")).isTrue();
		assertThat(DESCRIPTION_CONTAINS.test(rssItemDto, "esc")).isTrue();
		assertThat(DESCRIPTION_CONTAINS.test(rssItemDto, "desc")).isTrue();
		assertThat(DESCRIPTION_CONTAINS.test(rssItemDto, "sca")).isFalse();
	}

	@Test
	public void testTitleStartsWith() {
		assertThat(TITLE_STARTS_WITH.test(rssItemDto, "Tit")).isTrue();
		assertThat(TITLE_STARTS_WITH.test(rssItemDto, "tIT")).isTrue();
		assertThat(TITLE_STARTS_WITH.test(rssItemDto, "le")).isFalse();
	}

	@Test
	public void testTitleContains() {
		assertThat(TITLE_CONTAINS.test(rssItemDto, "Tit")).isTrue();
		assertThat(TITLE_CONTAINS.test(rssItemDto, "lE")).isTrue();
		assertThat(TITLE_CONTAINS.test(rssItemDto, "les")).isFalse();
	}

}
