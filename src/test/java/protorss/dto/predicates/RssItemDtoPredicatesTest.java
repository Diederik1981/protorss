package protorss.dto.predicates;

import static org.assertj.core.api.Assertions.assertThat;
import static protorss.dto.predicates.RssFeedDtoPredicates.DESCRIPTION_CONTAINS;
import static protorss.dto.predicates.RssFeedDtoPredicates.DESCRIPTION_STARTS_WITH;
import static protorss.dto.predicates.RssFeedDtoPredicates.TITLE_CONTAINS;
import static protorss.dto.predicates.RssFeedDtoPredicates.TITLE_STARTS_WITH;

import org.junit.Test;

import protorss.dto.RssFeedDto;

/**
 * Test for 'RssFeedDto' predicates
 */
public class RssItemDtoPredicatesTest {
	
	/**
	 * The Dto that will be used for all predicates.
	 */
	public static final RssFeedDto rssFeedDto = RssFeedDto.builder()
			.description("Description")
			.link("Link")
			.title("Title")
			.build();

	@Test
	public void testDescriptionStartsWith() {
		assertThat(DESCRIPTION_STARTS_WITH.test(rssFeedDto, "Desc")).isTrue();
		assertThat(DESCRIPTION_STARTS_WITH.test(rssFeedDto, "esc")).isFalse();
		assertThat(DESCRIPTION_STARTS_WITH.test(rssFeedDto, "desc")).isTrue();
	}

	@Test
	public void testDescriptionContains() {
		assertThat(DESCRIPTION_CONTAINS.test(rssFeedDto, "Desc")).isTrue();
		assertThat(DESCRIPTION_CONTAINS.test(rssFeedDto, "esc")).isTrue();
		assertThat(DESCRIPTION_CONTAINS.test(rssFeedDto, "desc")).isTrue();
		assertThat(DESCRIPTION_CONTAINS.test(rssFeedDto, "sca")).isFalse();
	}

	@Test
	public void testTitleStartsWith() {
		assertThat(TITLE_STARTS_WITH.test(rssFeedDto, "Tit")).isTrue();
		assertThat(TITLE_STARTS_WITH.test(rssFeedDto, "tIT")).isTrue();
		assertThat(TITLE_STARTS_WITH.test(rssFeedDto, "le")).isFalse();
	}

	@Test
	public void testTitleContains() {
		assertThat(TITLE_CONTAINS.test(rssFeedDto, "Tit")).isTrue();
		assertThat(TITLE_CONTAINS.test(rssFeedDto, "lE")).isTrue();
		assertThat(TITLE_CONTAINS.test(rssFeedDto, "les")).isFalse();
	}

}
