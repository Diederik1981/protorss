package protorss.misc;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class PaginationTest {
	
	@Test
	public void testPagination() {
		// given: i have 'pagination' values
		int pageNo = 4;
		int pageSize = 10;
		
		// when: i create a 'pagination'
		Pagination pagination = Pagination.of(pageNo, pageSize);
		
		// then: 'pageno' is one less and 'pagesize' is same
		assertThat(pagination.getPageNo()).isEqualTo(--pageNo);
		assertThat(pagination.getPageSize()).isEqualTo(pageSize);
	}
	@Test
	public void testPaginationNegativeNumbers() {
		// given: i have 'pagination' values
		int pageNo = -4;
		int pageSize = -10;
		
		// when: i create a 'pagination'
		Pagination pagination = Pagination.of(pageNo, pageSize);
		
		// then: 'pageno' is one less and 'pagesize' is same
		assertThat(pagination.getPageNo()).isEqualTo(0);
		assertThat(pagination.getPageSize()).isEqualTo(1);
	}
	
	
	@Test
	public void testPaginationMaxValue() {
		// given: i have 'pagination' values with a too large max
		int pageNo = 4;
		int pageSize = 8000;
		
		// when: i create a 'pagination'
		Pagination pagination = Pagination.of(pageNo, pageSize);
		
		// then: 'pageno' is one less and 'pagesize' is equal to default max
		assertThat(pagination.getPageNo()).isEqualTo(--pageNo);
		assertThat(pagination.getPageSize()).isEqualTo(Pagination.MAX_PAGE_SIZE);
	}
	
	@Test
	public void testPaginationLargerMaxValue() {
		// given: i have 'pagination' values with larger max
		int pageNo = 4;
		int pageSize = 8000;
		int largerAllowed = 16000;
		
		// when: i create a 'pagination' with larger allowed value
		Pagination pagination = Pagination.of(pageNo, pageSize, largerAllowed);
		
		// then: 'pageno' is one less and 'pagesize' is equal given pagesize
		assertThat(pagination.getPageNo()).isEqualTo(--pageNo);
		assertThat(pagination.getPageSize()).isEqualTo(pageSize);
	}
	
	
	@Test
	public void testPaginationTooSmallMaxValue() {
		// given: i have 'pagination' values with too small max value
		int pageNo = 4;
		int pageSize = 8000;
		int largerAllowed = -1;
		
		// when: i create a 'pagination'
		Pagination pagination = Pagination.of(pageNo, pageSize, largerAllowed);
		
		// then: 'pagesize' is 1
		assertThat(pagination.getPageNo()).isEqualTo(--pageNo);
		assertThat(pagination.getPageSize()).isEqualTo(1);
	}
	
	@Test
	public void testPaginationSkip() {
		// given: i have 'pagination' values:
		int pageNo = 4;
		int pageSize = 100;
		
		// when: i create a 'pagination'
		Pagination pagination = Pagination.of(pageNo, pageSize);
		
		// then: 'pagesize' is 1
		assertThat(pagination.getSkip()).isEqualTo(--pageNo * pageSize);
	}
	

}
