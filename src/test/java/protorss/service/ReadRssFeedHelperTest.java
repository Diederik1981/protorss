package protorss.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeedImpl;

import protorss.TestConstants;
import protorss.domain.RssFeed;
import protorss.service.exception.ReadRssFeedMalformedURLException;
import protorss.service.exception.ReadRssFeedNullPointerException;
import protorss.service.helper.ReadRssFeedHelper;

/**
 * Tests for the Rss Reader helper. This class relies on an active internet connection and an active Rss feed. 
 */
public class ReadRssFeedHelperTest {

	@Test(expected = ReadRssFeedNullPointerException.class)
	public void testURLNotNull() {
		ReadRssFeedHelper.readFeed(null);
	}
	
	@Test(expected = ReadRssFeedMalformedURLException.class)
	public void testURLMalformed() {
		ReadRssFeedHelper.readFeed("www.malformedrssfeed");
	}
	
	/**
	 * !! This test is dependant on internet access !!
	 */
	@Test
	public void testExternalFeedOK() {
		
		// when: you enter a legit 'url'
		RssFeed readFeed = ReadRssFeedHelper.readFeed(TestConstants.VALID_RSS_URL);
		
		// then: you get a 'feed' without errors
		assertThat(readFeed).isNotNull();
	}
	
	@Test
	public void testEmptySyndFeedToDomainOK() {
		
		// when: you convert an empty 'feed' to our domain
		RssFeed domain = ReadRssFeedHelper.toDomain(new SyndFeedImpl());
		
		// then: no error is thrown thus the domain is not null
		assertThat(domain).isNotNull();
	}
	
	@Test
	public void testSyndFeedWithEmptyEntryToDomainOK() {
		
		// given: a 'syndfeed' with an empty item 
		SyndFeedImpl syndFeed = new SyndFeedImpl();
		syndFeed.getEntries().add(new SyndEntryImpl());
		
		// when: you convert the 'feed' to our domain
		RssFeed domain = ReadRssFeedHelper.toDomain(syndFeed);
		
		// then: no error is thrown so the domain is not null
		assertThat(domain).isNotNull();;
	}

}
