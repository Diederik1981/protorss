package protorss.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.mockito.InjectMocks;

import protorss.TestConstants;
import protorss.domain.RssFeed;

public class ReadRssFeedServiceImplTest {
	
	@InjectMocks
	private ReadRssFeedServiceImpl m_service = new ReadRssFeedServiceImpl();

	@Test
	public void testSave() {
		// given: a valid 'rss feed' url
		String url = TestConstants.VALID_RSS_URL;
		
		// when: you 'read' the 'url'
		RssFeed feed = m_service.readUrl(url);

		// then: you get a 'rss feed'
		assertThat(feed).isNotNull();
	}
}
