package protorss.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import protorss.TestConstants;
import protorss.domain.RssFeed;
import protorss.domain.RssItem;
import protorss.dto.RssFeedDto;
import protorss.dto.RssItemDto;
import protorss.repository.RssFeedRepository;
import protorss.service.exception.RssChannelAlreadyAddedException;


@RunWith(MockitoJUnitRunner.class)
public class RssFeedServiceImplTest {
	
	@Mock
	private RssFeedRepository rssFeedRepository;

	@InjectMocks
	private RssFeedServiceImpl service = new RssFeedServiceImpl();
	
	@Test
	public void testSaveRssFeedEntity() {
		
		// given: an 'rssFeed' instance
		RssFeed feed = new RssFeed();
		feed.setTitle("feedTitle");
		
		// when: the 'rssFeed' is saved
		when(rssFeedRepository.saveAndFlush(feed)).thenReturn(feed);
		service.save(feed);
		
		// then: the 'repository' is called
		verify(rssFeedRepository).saveAndFlush(feed);
	}
	
	@Test
	public void testAddChannel() {
		// given: a valid 'rss url'
		String rssurl = TestConstants.VALID_RSS_URL;
		
		// when: the 'rssurl' is added
		when(rssFeedRepository.findByLink(rssurl)).thenReturn(Optional.empty());
		service.addChannel(rssurl);
		
		// then the correct beans are called
		verify(rssFeedRepository).findByLink(rssurl);
		verify(rssFeedRepository).saveAndFlush(Mockito.any());

	}
	
	@Test(expected = RssChannelAlreadyAddedException.class)
	public void testChannelMayNotAlreadyExist() {
		
		// given: a valid 'rss url'
		String rssurl = TestConstants.VALID_RSS_URL;
		
		// when: the 'rssurl' is added
		when(rssFeedRepository.findByLink(rssurl)).thenReturn(Optional.of(new RssFeed()));
		service.addChannel(rssurl);
	}
	
	@Test
	public void testCacheInitializesProperly() {
		// given the repo contains two items.
		when(rssFeedRepository.findAll()).thenReturn(List.of(
				RssFeed.builder().title("feed_title1").build(),
				RssFeed.builder().title("feed_title2").build()
				));
		
		// when: i request a stream from the 'cache' ( through a service method ).
		List<RssFeedDto> allFeeds1 = service.getAllFeeds(null, null);
		
		// then: the return array has '2' elements.
		assertThat(allFeeds1).hasSize(2);
		
		// and when: i request another
		List<RssFeedDto> allFeeds2 = service.getAllFeeds(null, null);
		verify(rssFeedRepository).findAll();
		
		// then: the responce is equal
		assertThat(allFeeds1.toString()).isEqualTo(allFeeds2.toString());
		// and: the repo is 'not' called again
		verifyNoMoreInteractions(rssFeedRepository);

	}
	
	@Test
	public void testCacheAcceptsNewValues() {
		// given: the 'repo' contains two 'items'.
		when(rssFeedRepository.findAll()).thenReturn(List.of(
				RssFeed.builder().title("feed_title1").build(),
				RssFeed.builder().title("feed_title2").build()
				));
		// when: i save a 'feed'
		String newTitle = "feed_title3";
		service.save(RssFeed.builder().title(newTitle).build());
		
		// then: there are 3 'feeds' in the cache and one is the one i put in.
		List<RssFeedDto> allFeeds = service.getAllFeeds(null, null);
		assertThat(allFeeds)
			.hasSize(3)
			.anyMatch(f -> f.getTitle().equals(newTitle));
		
		// and when: i save another feed
		String newTitle2 = "feed_title4";
		service.save(RssFeed.builder().title(newTitle2).build());
		
		// then: there are 3 'feeds' in the cache and one is the one i put in.
		List<RssFeedDto> allFeeds2 = service.getAllFeeds(null, null);
		assertThat(allFeeds2)
		.hasSize(4)
		.anyMatch(f -> f.getTitle().equals(newTitle2));
		
		// and: the repo is 'not' called again
		verify(rssFeedRepository, times(1)).findAll();
	}
	
	@Test
	public void testGetAllFeeds() {
		// given: the 'repo' contains two 'feeds'.
		when(rssFeedRepository.findAll()).thenReturn(List.of(
				RssFeed.builder().title("feed_title1").build(),
				RssFeed.builder().title("feed_title2").build()
				)); 
		
		// when: i get all 'feeds'
		List<RssFeedDto> allFeeds = service.getAllFeeds(null, null);
		
		// then: the result has 2 'feeds' and one is the first added
		assertThat(allFeeds)
			.hasSize(2)
			.anyMatch(f -> f.getTitle().equals("feed_title1"));
	}
	
	@Test
	public void testGetAllItems() {
		// given: the 'repo' contains two 'feeds' with one item.
		when(rssFeedRepository.findAll()).thenReturn(List.of(
				RssFeed.builder()
					.title("feed_title1")
					.items(List.of(RssItem.builder()
							.title("feed1item1")
							.build()))
					.build(),
				RssFeed.builder()
					.title("feed_title2")
					.items(List.of(RssItem.builder()
							.title("feed2item1")
							.build()))
					.build()
				));
		// when: i get all 'items'
		List<RssItemDto> items = service.getAllItems(null, null);
		
		// then: the result has 2 'items' that were added above
		assertThat(items)
			.hasSize(2)
			.anyMatch(f -> f.getTitle().equals("feed1item1"))
			.anyMatch(f -> f.getTitle().equals("feed2item1"));
		
		
	}
	
}
