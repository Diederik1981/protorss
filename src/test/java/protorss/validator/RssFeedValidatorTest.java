package protorss.validator;

import org.junit.Test;

import protorss.domain.RssFeed;
import protorss.domain.RssItem;
import protorss.service.exception.ImportRssFeedHeaderRequiredFieldMissingException;
import protorss.service.exception.ImportRssFeedItemRequiredFieldMissingException;
import protorss.service.validator.RssFeedValidator;

/**
 * TestClass for 'RssFeed validation'. I am used to use testNG @DataProvider for cases like this, but i decided to do it this way.
 */
public class RssFeedValidatorTest {
	
	@Test
	public void TestValidRssFeed( ) {
		// no error is thrown when you 'validate' a valid 'rss feed'
		RssFeed feed = RssFeed.builder()
			.link("feed_link")
			.title("feed_title")
			.description("feed_description")
			.build();
		RssFeedValidator.validate(feed);
	}
	
	@Test(expected = ImportRssFeedItemRequiredFieldMissingException.class)
	public void TestValidRssFeedWithInvalidItemThrowsException( ) {
		// test that Feed tests its 'item' children 
		RssFeed feed = RssFeed.builder()
				.link("feed_link")
				.title("feed_title")
				.description("feed_description")
				.build();
		feed.getItems().add(RssItem.builder()
				.link("item_link")
				.title("item_title")
				.description(null)
				.build());
		RssFeedValidator.validate(feed);
	}
	
	@Test(expected = ImportRssFeedHeaderRequiredFieldMissingException.class)
	public void TestFeedNeedsLink() {
		RssFeed feed = RssFeed.builder()
				.link(null)
				.title("feed_title")
				.description("feed_description")
				.build();
		RssFeedValidator.validate(feed);
	}
	
	@Test(expected = ImportRssFeedHeaderRequiredFieldMissingException.class)
	public void TestFeedNeedsTitle() {
		RssFeed feed = RssFeed.builder()
				.link("feed_link")
				.title(null)
				.description("feed_description")
				.build();
		RssFeedValidator.validate(feed);
	}
	
	@Test(expected = ImportRssFeedHeaderRequiredFieldMissingException.class)
	public void TestFeedNeedsDescription() {
		RssFeed feed = RssFeed.builder()
				.link("feed_link")
				.title("feed_title")
				.description(null)
				.build();
		RssFeedValidator.validate(feed);
	}
	
	@Test
	public void TestValidRssItemThrowsNoException() {
		// no error is thrown when you 'validate' a valid 'rss item'
		RssItem item = RssItem.builder()
			.link("item_link")
			.title("item_title")
			.description("item_description")
			.build();
		RssFeedValidator.validate(item);
	}
	
	@Test(expected = ImportRssFeedItemRequiredFieldMissingException.class)
	public void TestItemNeedsLink() {
		RssItem item = RssItem.builder()
				.link(null)
				.title("item_title")
				.description("item_description")
				.build();
		RssFeedValidator.validate(item);
	}
	
	@Test(expected = ImportRssFeedItemRequiredFieldMissingException.class)
	public void TestItemNeedsTitle() {
		RssItem item = RssItem.builder()
				.link("item_link")
				.title(null)
				.description("item_description")
				.build();
		RssFeedValidator.validate(item);
	}
	
	@Test(expected = ImportRssFeedItemRequiredFieldMissingException.class)
	public void TestItemNeedsDescription() {
		RssItem item = RssItem.builder()
				.link("item_link")
				.title("item_title")
				.description(null)
				.build();
		RssFeedValidator.validate(item);
	}

}
